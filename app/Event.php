<?php

namespace App;

use App\Exceptions\NotEnoughTicketsException;
use App\Reservation;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	// Change this in the future
    protected $guarded = [];

    protected $dates = ['date'];

    public function scopePublished($query)
    {
    	return $query->whereNotNull('published_at');
    }

    public function getFormattedDateAttribute()
    {
    	return $this->date->format('F j, Y');
    }

    public function getFormattedStartTimeAttribute()
    {
    	return $this->date->format('g:ia');
    }

    public function getPriceInDollarsAttribute()
    {
    	return number_format($this->ticket_price / 100, 2);
    }

    public function ticketPriceInDollars($ticketType)
    {
        return number_format($this->tickets()->where('type', $ticketType)->first()->price / 100, 2);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'tickets');
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function reserveTickets($quantity, $type, $email)
    {
        $tickets = $this->findTickets($quantity, $type)->each(function ($ticket) {
            $ticket->reserve();
        });

        return new Reservation($tickets, $email);
    }

    public function findTickets($quantity, $type)
    {
        $tickets = $this->tickets()->available($type)->take($quantity)->get();

        if ($tickets->count() < $quantity) {
            throw new NotEnoughTicketsException;
        }

        return $tickets;
    }

    public function addTickets($quantity, $type, $price)
    {
        foreach (range(1, $quantity) as $i) {
            $this->tickets()->create([
                'type' => $type,
                'price' => $price,
            ]);
        }
    }

    public function ticketsRemaining($type = null)
    {
        return $this->tickets()->available($type)->count();
    }

    public function ticketTypes()
    {
        return $this->tickets->pluck('price', 'type');
    }

    public function getTicketTypesAttribute()
    {
        return $this->ticketTypes()->map(function ($price) {
            return number_format($price / 100, 2);
        });
    }
}
