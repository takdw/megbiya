<?php

namespace App;

use App\Facades\TicketCode;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	protected $guarded = [];

    public function scopeAvailable($query, $type = null)
    {
        $query = $query->whereNull('order_id')->whereNull('reserved_at');

        if ($type !== null) {
            return $query->where('type', $type);
        }

        return $query;
    }

    public function reserve()
    {
        $this->update(['reserved_at' => now()]);
    }

    public function release()
    {
    	$this->update(['reserved_at' => null]);
    }

    public function claimFor($order)
    {
        $this->code = TicketCode::generateFor($this);
        $order->tickets()->save($this);
    }

    public function event()
    {
    	return $this->belongsTo(Event::class);
    }
}
