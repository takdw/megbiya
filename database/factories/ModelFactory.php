<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(\App\Event::class, function (Faker $faker) {
	return [
        'title' => 'The Test Event',
        'subtitle' => 'with Laravel Testing',
        'date' => Carbon::parse('+1 week'),
        // 'ticket_price' => 2000,
        'venue' => 'The Example Arena',
        'venue_address' => '123 Example Lane',
        'city' => 'Fakeville',
        'state' => 'LR',
        'zip' => '90210',
        'additional_information' => 'Sample additional information',
    ];
});

$factory->state(\App\Event::class, 'published', function (Faker $faker) {
    return [
        'published_at' => Carbon::parse('-1 week'),
    ];
});

$factory->state(\App\Event::class, 'unpublished', function (Faker $faker) {
    return [
        'published_at' => null,
    ];
});

$factory->define(\App\Ticket::class, function (Faker $faker) {
    return [
        'event_id' => function () {
            return factory(\App\Event::class)->create()->id;
        },
        'type' => ucfirst($faker->word),
        'price' => 5700
    ];
});

$factory->state(\App\Ticket::class, 'reserved', function (Faker $faker) {
    return [
        'reserved_at' => now(),
    ];
});

$factory->define(\App\Order::class, function (Faker $faker) {
    return [
        'confirmation_number' => 'ORDERCONFIRMATION1234',
        'amount' => 9999,
        'email' => 'dummy@example.com',
        'card_last_four' => '1234',
    ];
});