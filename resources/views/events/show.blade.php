@extends('layouts.app')

@section('content')
<div class="page min-h-screen w-full bg-gray-200 text-gray-800 flex justify-center items-center">
	<div class="max-w-lg w-full px-8 py-6 rounded-lg bg-white shadow">
		<div class="mb-4 leading-tight border-b pb-4">
			<h1 class="text-3xl font-bold">{{ $event->title }}</h1>
			<h2 class="text-xl">{{ $event->subtitle }}</h2>
		</div>
		<div>
			<p class="flex py-2">
				<svg class="fill-current h-5 w-5 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M1 4c0-1.1.9-2 2-2h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4zm2 2v12h14V6H3zm2-6h2v2H5V0zm8 0h2v2h-2V0zM5 9h2v2H5V9zm0 4h2v2H5v-2zm4-4h2v2H9V9zm0 4h2v2H9v-2zm4-4h2v2h-2V9zm0 4h2v2h-2v-2z"/></svg>
				<span class="font-semibold">{{ $event->formatted_date }}</span>
			</p>
			<p class="flex py-2">
				<svg class="fill-current h-5 w-5 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-1-7.59V4h2v5.59l3.95 3.95-1.41 1.41L9 10.41z"/></svg>
				<span class="font-semibold">Doors at {{ $event->formatted_start_time }}</span>
			</p>
			<p class="flex py-2">
				<svg class="fill-current h-5 w-5 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm1-5h1a3 3 0 0 0 0-6H7.99a1 1 0 0 1 0-2H14V5h-3V3H9v2H8a3 3 0 1 0 0 6h4a1 1 0 1 1 0 2H6v2h3v2h2v-2z"/></svg>
				<span class="flex">
				@foreach($event->ticketTypes as $type => $price)
				<span class="flex flex-col leading-tight mr-6">
					<span class="text-gray-600 uppercase text-sm">{{ $type }}</span>
					<span><span class="text-xl font-bold">{{ $price }}</span></span>
				</span>
				@endforeach
			</p>
			<p class="flex py-2">
				<svg class="fill-current h-5 w-5 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20S3 10.87 3 7a7 7 0 1 1 14 0c0 3.87-7 13-7 13zm0-11a2 2 0 1 0 0-4 2 2 0 0 0 0 4z"/></svg>
				<span class="flex flex-col">
					<span class="font-semibold">{{ $event->venue }}</span>
					<span class="text-gray-600">{{ $event->venue_address }}</span>
					<span class="text-gray-600">{{ $event->city }}, {{ $event->state }} {{ $event->zip }}</span>
				</span>
			</p>
			<p class="flex py-2">
				<svg class="fill-current h-5 w-5 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zM9 11v4h2V9H9v2zm0-6v2h2V5H9z"/></svg>
				<span class="flex flex-col">
					<span class="font-semibold">Additional Information</span>
					<span class="text-gray-600">{{ $event->additional_information }}</span>
				</span>
			</p>
			<buy-tickets-section :event="{{ $event }}" :ticket-types="{{ $event->ticketTypes() }}"></buy-tickets-section>
		</div>
		
	</div>
</div>

@endsection