<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Ticketing</title>

        <!-- CSS -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap" rel="stylesheet">

    </head>
    <body class="font-display anitaliased">
        <div id="app" class="relative">
            <main>
                @yield('content')
            </main>
            
            <portal-target name="modals"></portal-target>
        </div>
        <script src="https://js.stripe.com/v3/"></script>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
