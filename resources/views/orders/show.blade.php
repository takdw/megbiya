<p>Confirmation Number: {{ $order->confirmation_number }}</p>
<p>Paid Amount: ${{ number_format($order->amount / 100, 2) }}</p>
<p>Used Card Number: **** **** **** 1881</p>
<ul>
	@foreach($order->tickets as $ticket)
	<li>
		<a href="#">{{ $ticket->code }}</a>
		<h2>{{ $ticket->event->title }}</h2>
		<p>{{ $ticket->event->subtitle }}</p>
		<p>{{ $ticket->event->date->format('l, F j, Y') }}</p>
		<p>{{ $ticket->event->venue }}</p>
		<p>{{ $ticket->event->venue_address }}</p>
		<p>{{ $ticket->event->city }}, {{ $ticket->event->state }}</p>
		<p>{{ $ticket->event->zip }}</p>
		<p>Doors at {{ $ticket->event->date->format('g:ia') }}</p>
		<p>Doors at {{ $order->email }}</p>
	</li>
	@endforeach
</ul>