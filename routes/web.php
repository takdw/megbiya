<?php

Route::get('/events/{id}', 'EventsController@show');
Route::post('/events/{id}/orders', 'EventOrdersController@store');

Route::get('/orders/{confirmationNumber}', 'OrdersController@show');