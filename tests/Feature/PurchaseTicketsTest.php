<?php

namespace Tests\Feature;

use App\Billing\FakePaymentGateway;
use App\Billing\PaymentGateway;
use App\Event;
use App\Facades\OrderConfirmationNumber;
use App\Facades\TicketCode;
use App\Mail\OrderConfirmationEmail;
use App\OrderConfirmationNumberGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class PurchaseTicketsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->paymentGateway = new FakePaymentGateway;
        $this->app->instance(PaymentGateway::class, $this->paymentGateway);
        Mail::fake();
    }

    /** @test */
    function customers_can_purchase_tickets_to_a_published_event()
    {
        $this->withoutExceptionHandling();

        OrderConfirmationNumber::shouldReceive('generate')->andReturn('ORDERCONFIRMATION1234');
        TicketCode::shouldReceive('generateFor')->andReturn('TICKETCODE1', 'TICKETCODE2', 'TICKETCODE3');

        $event = factory(Event::class)->states('published')->create();
        $event->addTickets(3, 'VIP', 5000);

        $response = $this->json('POST', "/events/{$event->id}/orders", [
            'email' => 'john@example.com',
            'ticket_quantity' => 3,
            'ticket_type' => 'VIP',
            'payment_token' => $this->paymentGateway->getValidTestToken(),
        ]);

        $response->assertStatus(201);
        $response->assertJson([
            'confirmation_number' => 'ORDERCONFIRMATION1234',
            'email' => 'john@example.com',
            'amount' => 15000,
            'tickets' => [
                [
                    'code' => 'TICKETCODE1',
                    'type' => 'VIP',
                ],
                [
                    'code' => 'TICKETCODE2',
                    'type' => 'VIP',
                ],
                [
                    'code' => 'TICKETCODE3',
                    'type' => 'VIP'
                ],
            ],
        ]);
        $this->assertEquals(15000, $this->paymentGateway->totalCharges());
        
        $order = $event->orders()->where('email', 'john@example.com')->first();
        $this->assertNotNull($order);
        $this->assertEquals(3, $order->tickets->count());
        Mail::assertSent(OrderConfirmationEmail::class, function ($mail) use ($order) {
            return $mail->hasTo('john@example.com')
                && $mail->order->id == $order->id;
        });
    }

    /** @test */
    function customers_cannot_purchase_tickets_to_an_unpublished_event()
    {
        $event = factory(Event::class)->states('unpublished')->create();
        $event->addTickets(3, 'Regular', 1200);

        $response = $this->json('POST', "/events/{$event->id}/orders", [
            'email' => 'john@example.com',
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
        ]);

        $response->assertStatus(404);
        $this->assertEquals(0, $event->orders->count());
        $this->assertEquals(0, $this->paymentGateway->totalCharges());
    }

    /** @test */
    function an_order_is_not_created_if_payment_fails()
    {
        $event = factory(Event::class)->states('published')->create();
        $event->addTickets(3, 'Regular', 1200);

        $response = $this->json('POST', "/events/{$event->id}/orders", [
            'email' => 'john@example.com',
            'ticket_quantity' => 3,
            'payment_token' => 'invalid-payment-token',
        ]);

        $response->assertStatus(422);
        $order = $event->orders()->where('email', 'john@example.com')->first();
        $this->assertNull($order);
        $this->assertEquals(3, $event->ticketsRemaining());
    }

    /** @test */
    function email_is_required_to_purchase_tickets()
    {
        $event = factory(Event::class)->states('published')->create();

        $response = $this->json('POST', "/events/{$event->id}/orders", [
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }

    /** @test */
    function email_must_be_valid_to_purchase_tickets()
    {
        $event = factory(Event::class)->states('published')->create();

        $response = $this->json('POST', "/events/{$event->id}/orders", [
            'email' => 'bad-email-address',
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }

    /** @test */
    function ticket_quantity_is_required_to_purchase_tickets()
    {
        $event = factory(Event::class)->states('published')->create();

        $response = $this->json('POST', "/events/{$event->id}/orders", [
            'email' => 'john@example.com',
            'payment_token' => $this->paymentGateway->getValidTestToken(),
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('ticket_quantity');
    }

    /** @test */
    function ticket_quantity_must_be_at_least_one_to_purchase_tickets()
    {
        $event = factory(Event::class)->states('published')->create();

        $response = $this->json('POST', "/events/{$event->id}/orders", [
            'email' => 'john@example.com',
            'ticket_quantity' => 0,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('ticket_quantity');
    }

    /** @test */
    function a_payment_token_is_required_to_purchase_tickets()
    {
        $event = factory(Event::class)->states('published')->create();

        $response = $this->json('POST', "/events/{$event->id}/orders", [
            'email' => 'john@example.com',
            'ticket_quantity' => 3,
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('payment_token');
    }

    /** @test */
    function cannot_purchase_more_tickets_than_remaining()
    {
        $event = factory(Event::class)->states('published')->create();
        $event->addTickets(5, 'Regular', 1200);

        $response = $this->json('POST', "/events/{$event->id}/orders", [
            'email' => 'john@example.com',
            'ticket_quantity' => 6,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
        ]);

        $response->assertStatus(422);
        $order = $event->orders()->where('email', 'john@example.com')->first();
        $this->assertNull($order);
        $this->assertEquals(0, $this->paymentGateway->totalCharges());
        $this->assertEquals(5, $event->ticketsRemaining());
    }

    /** @test */
    function cannot_purchase_tickets_another_customer_is_already_trying_to_purchase()
    {
        $this->withoutExceptionHandling();
        $event = factory(Event::class)->states('published')->create();
        $event->addTickets(3, 'Regular', 5000);

        $this->paymentGateway->beforeFirstCharge(function ($paymentGateway) use ($event) {
            // Backing up John's request because Laravel keeps only a single reference to the request
            $johnsRequest = $this->app['request'];

            $janesResponse = $this->json('POST', "/events/{$event->id}/orders", [
                'email' => 'jane@example.com',
                'ticket_quantity' => 1,
                'ticket_type' => 'Regular',
                'payment_token' => $paymentGateway->getValidTestToken(),
            ]);

            $this->app['request'] = $johnsRequest;

            $janesResponse->assertStatus(422);
            $janesOrder = $event->orders()->where('email', 'jane@example.com')->first();
            $this->assertNull($janesOrder);
            $this->assertEquals(0, $paymentGateway->totalCharges());
        });

        $johnsResponse = $this->json('POST', "/events/{$event->id}/orders", [
            'email' => 'john@example.com',
            'ticket_quantity' => 3,
            'ticket_type' => 'Regular',
            'payment_token' => $this->paymentGateway->getValidTestToken(),
        ]);

        $this->assertEquals(15000, $this->paymentGateway->totalCharges());
        $johnsOrder = $event->orders()->where('email', 'john@example.com')->first();
        $this->assertNotNull($johnsOrder);
        $this->assertEquals(3, $johnsOrder->tickets->count());
    }
}
