<?php

namespace Tests\Feature;

use App\Event;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ViewEventListingTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function users_can_view_a_published_event_listings()
    {
        $this->withoutExceptionHandling();

        $event = factory(Event::class)->states('published')->create([
            'title' => 'The Red Chord',
            'subtitle' => 'with Animosity and Lethargy',
            'date' => Carbon::parse('December 16, 2019 8:00pm'),
            'venue' => 'The Laravel Arena',
            'venue_address' => '123 Example Lane',
            'city' => 'Laraville',
            'state' => 'ON',
            'zip' => '90210',
            'additional_information' => 'For tickets, call (555) 555-5555',
        ]);
        $event->addTickets(40, 'Regular', 5900);
        $event->addTickets(10, 'Couple', 6900);
        $event->addTickets(10, 'VIP', 9900);

        $response = $this->get('/events/'.$event->id);

        $response->assertStatus(200);
        $response->assertSee('The Red Chord');
        $response->assertSee('with Animosity and Lethargy');
        $response->assertSee('December 16, 2019');
        $response->assertSee('8:00pm');
        $response->assertSee('Regular');
        $response->assertSee('Couple');
        $response->assertSee('VIP');
        $response->assertSee('59.00');
        $response->assertSee('69.00');
        $response->assertSee('99.00');
        $response->assertSee('The Laravel Arena');
        $response->assertSee('123 Example Lane');
        $response->assertSee('Laraville, ON 90210');
        $response->assertSee('For tickets, call (555) 555-5555');
    }

    /** @test */
    function users_cannot_view_unpublished_event_listings()
    {
        $event = factory(Event::class)->states('unpublished')->create();

        $response = $this->get('/events/'.$event->id);

        $response->assertStatus(404);
    }
}
