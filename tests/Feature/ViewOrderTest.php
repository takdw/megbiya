<?php

namespace Tests\Feature;

use App\Event;
use App\Order;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ViewOrderTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function user_can_view_their_order_confirmation()
    {
        $this->withoutExceptionHandling();

        $event = factory(Event::class)->create([
            'title' => 'The Cool Event',
            'subtitle' => 'featuring Sub Zero',
            'date' => Carbon::parse('November 27, 2019 8:00pm'),
            'venue' => 'The Addis Arena',
            'venue_address' => '123 Sheger Lane',
            'city' => 'Aradaville',
            'state' => 'AA',
            'zip' => '10381',
            'additional_information' => 'Get over here... Oh wait... That is Scorpion',
        ]);
        $order = factory(Order::class)->create([
            'confirmation_number' => 'ORDERCONFIRMATION1234',
            'email' => 'recker@example.com',
            'amount' => 9999,
            'card_last_four' => 1881,
        ]);
        $ticketA = factory(Ticket::class)->create([
            'event_id' => $event->id,
            'order_id' => $order->id,
            'code' => 'TICKETCODE123',
        ]);
        $ticketB = factory(Ticket::class)->create([
            'event_id' => $event->id,
            'order_id' => $order->id,
            'code' => 'TICKETCODE456',
        ]);

        $response = $this->get("/orders/ORDERCONFIRMATION1234");

        $response->assertStatus(200);
        $response->assertViewHas('order', $order);
        $response->assertSee('ORDERCONFIRMATION1234');
        $response->assertSee('$99.99');
        $response->assertSee('**** **** **** 1881');
        $response->assertSee('TICKETCODE123');
        $response->assertSee('TICKETCODE456');
        $response->assertSee('The Cool Event');
        $response->assertSee('featuring Sub Zero');
        $response->assertSee('Wednesday, November 27, 2019');
        $response->assertSee('The Addis Arena');
        $response->assertSee('123 Sheger Lane');
        $response->assertSee('Aradaville, AA');
        $response->assertSee('10381');
        $response->assertSee('Doors at 8:00pm');
        $response->assertSee('recker@example.com');        
    }
}
