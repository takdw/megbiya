<?php

namespace Tests\Unit;

use App\Event;
use App\Exceptions\NotEnoughTicketsException;
use App\Order;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EventTest extends TestCase
{
	use RefreshDatabase;
	
    /** @test */
    function can_get_formatted_date()
    {
		$event = factory(Event::class)->make([
            'date' => Carbon::parse('2019-12-01 8:00pm'),
        ]);	

        $this->assertEquals('December 1, 2019', $event->formatted_date);
    }

    /** @test */
    function can_get_formatted_start_time()
    {
    	$event = factory(Event::class)->make([
            'date' => Carbon::parse('2019-12-01 17:00:00'),
        ]);

        $this->assertEquals('5:00pm', $event->formatted_start_time);
    }

    /** @test */
    function can_get_formatted_ticket_prices()
    {
    	$event = factory(Event::class)->create();
        $event->addTickets(7, 'Regular', 1000);
        $event->addTickets(4, 'VIP', 2000);

        $this->assertEquals('10.00', $event->ticketPriceInDollars('Regular'));
        $this->assertEquals('20.00', $event->ticketPriceInDollars('VIP'));
    }

    /** @test */
    function events_with_a_published_at_date_are_published()
    {
    	$publishedEventA = factory(Event::class)->create(['published_at' => Carbon::parse('-1 week')]);
    	$publishedEventB = factory(Event::class)->create(['published_at' => Carbon::parse('-1 week')]);
    	$unpublishedEvent = factory(Event::class)->create(['published_at' => null]);

    	$publishedEvents = Event::published()->get();

    	$this->assertTrue($publishedEvents->contains($publishedEventA));
    	$this->assertTrue($publishedEvents->contains($publishedEventB));
    	$this->assertFalse($publishedEvents->contains($unpublishedEvent));
    }

    /** @test */
    function can_add_tickets_to_an_event()
    {
        $event = factory(Event::class)->create();
        
        $event->addTickets(5, 'Regular', 1200);
        $event->addTickets(4, 'VIP', 2000);

        $this->assertEquals(5, $event->ticketsRemaining('Regular'));
        $this->assertEquals(4, $event->ticketsRemaining('VIP'));
        $this->assertEquals(9, $event->ticketsRemaining());
    }

    /** @test */
    function tickets_remaining_does_not_include_tickets_associated_with_an_order()
    {
        $event = factory(Event::class)->create();
        $event->tickets()->saveMany(factory(Ticket::class, 3)->create(['order_id' => 1]));
        $event->tickets()->saveMany(factory(Ticket::class, 2)->create(['order_id' => null]));

        $this->assertEquals(2, $event->ticketsRemaining());
    }

    /** @test */
    function trying_to_reserve_more_tickets_than_remaining_throws_an_exception()
    {
        $event = factory(Event::class)->create();
        $event->addTickets(5, 'VIP', 1000);

        try {
            $event->reserveTickets(6, 'VIP', 'john@example.com');
        } catch (NotEnoughTicketsException $e) {
            $order = $event->orders()->where('email', 'jane@example.com')->first();
            $this->assertNull($order);
            $this->assertEquals(5, $event->ticketsRemaining());
            return;
        }

        $this->fail('Ticket order succeded even though there were not enough tickets remaining.');
    }

    /** @test */
    function an_event_can_reserve_available_tickets()
    {
        $event = factory(Event::class)->create();
        $event->addTickets(5, 'Regular', 2600);
        $this->assertEquals(5, $event->ticketsRemaining());

        $reservation = $event->reserveTickets(3, 'Regular', 'john@example.com');

        $this->assertCount(3, $reservation->tickets());
        $this->assertEquals('john@example.com', $reservation->email());
        $this->assertEquals(2, $event->ticketsRemaining());
    }

    /** @test */
    function cannot_reserve_tickets_that_have_already_been_purchased()
    {
        $event = factory(Event::class)->create();
        $event->addTickets(5, 'Regular', 1000);
        $order = factory(Order::class)->create();
        $order->tickets()->saveMany($event->tickets->take(2));

        try {
            $event->reserveTickets(4, 'Regular', 'jane@example.com');
        } catch (NotEnoughTicketsException $e) {
            $this->assertEquals(3, $event->ticketsRemaining());
            return;
        }

        $this->fail("Reserving tickets succeded even though the tickets were already sold!");
    }

    /** @test */
    function cannot_reserve_tickets_that_have_already_been_reserved()
    {
        $event = factory(Event::class)->create();
        $event->addTickets(5, 'Type', 2000);
        $event->reserveTickets(2, 'Type', 'jane@example.com');

        try {
            $event->reserveTickets(4, 'Type', 'john@example.com');
        } catch (NotEnoughTicketsException $e) {
            $this->assertEquals(3, $event->ticketsRemaining());
            return;
        }

        $this->fail("Reserving tickets succeded even though the tickets were already reserved!");
    }

    /** @test */
    function can_get_the_ticket_types()
    {
        $event = factory(Event::class)->create();
        $event->addTickets(7, 'Type 1', 2500);
        $event->addTickets(3, 'Type 2', 6500);
        $event->addTickets(3, 'Type 3', 9500);

        $ticketTypes = $event->ticketTypes();

        $this->assertEquals([
            'Type 1' => 2500,
            'Type 2' => 6500,
            'Type 3' => 9500,
        ], $ticketTypes->toArray());
    }

    /** @test */
    function can_get_the_ticket_types_with_formatted_price()
    {
        $event = factory(Event::class)->create();
        $event->addTickets(7, 'Type 1', 2449);
        $event->addTickets(3, 'Type 2', 6999);

        $ticketTypes = $event->ticketTypes;

        $this->assertEquals([
            'Type 1' => '24.49',
            'Type 2' => '69.99',
        ], $ticketTypes->toArray());
    }
}
