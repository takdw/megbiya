<?php

namespace Tests\Unit;

use App\Billing\Charge;
use App\Event;
use App\Exceptions\NotEnoughTicketsException;
use App\Order;
use App\Reservation;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    function creating_an_order_from_tickets_email_and_charge()
    {
        $tickets = collect([
            \Mockery::spy(Ticket::class),
            \Mockery::spy(Ticket::class),
            \Mockery::spy(Ticket::class),
        ]);

        $charge = new Charge([
            'amount' => 9000,
            'card_last_four' => '1234',
        ]);

        $order = Order::forTickets($tickets, 'john@example.com', $charge);

        $this->assertEquals('john@example.com', $order->email);
        $this->assertEquals(9000, $order->amount);
        $this->assertEquals('1234', $order->card_last_four);
        $tickets->each->shouldHaveReceived('claimFor', [$order]);
    }

    /** @test */
    function converting_to_an_array()
    {
        $order = factory(Order::class)->create([
            'email' => 'jane@example.com',
            'amount' => 10000,
            'confirmation_number' => 'ORDERCONFIRMATION1234',
        ]);
        $order->tickets()->saveMany([
            factory(Ticket::class)->create(['type' => 'Regular', 'code' => 'TICKETCODE1']),
            factory(Ticket::class)->create(['type' => 'VIP', 'code' => 'TICKETCODE2']),
            factory(Ticket::class)->create(['type' => 'Regular', 'code' => 'TICKETCODE3']),
        ]);

        $result = $order->toArray();

        $this->assertEquals([
            'confirmation_number' => 'ORDERCONFIRMATION1234',
            'email' => 'jane@example.com',
            'amount' => 10000,
            'tickets' => [
                ['type' => 'Regular', 'code' => 'TICKETCODE1'],
                ['type' => 'VIP', 'code' => 'TICKETCODE2'],
                ['type' => 'Regular', 'code' => 'TICKETCODE3'],
            ],
        ], $result);
    }

    /** @test */
    function fetching_an_order_by_confirmation_number()
    {
        $order = factory(Order::class)->create([
            'confirmation_number' => 'ORDERCONFIRMATION1234',
        ]);

        $foundOrder = Order::findByConfirmationNumber('ORDERCONFIRMATION1234');

        $this->assertEquals($order->fresh(), $foundOrder);
    }

    /** @test */
    function fetching_a_nonexistent_order_by_confirmation_number_throws_an_exception()
    {
        try {
            Order::findByConfirmationNumber('NONEXISTENTCONFIRMATIONNUMBER');
        } catch (ModelNotFoundException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('No matching order was found but and exception was not thrown.');
    }
}
