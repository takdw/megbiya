<?php

namespace Tests\Unit;

use App\Billing\FakePaymentGateway;
use App\Event;
use App\Reservation;
use App\Ticket;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReservationTest extends TestCase
{
	use RefreshDatabase;
	
    /** @test */
    function can_calculate_the_total_cost()
    {
    	$event = factory(Event::class)->create();
    	$event->addTickets(10, 'Some Type', 1500);
    	$tickets = $event->findTickets(4, 'Some Type');

    	$reservation = new Reservation($tickets, 'john@example.com');

    	$this->assertEquals(6000, $reservation->totalCost());
    }

    /** @test */
    function reserved_tickets_are_released_when_a_reservation_is_cancelled()
    {
        $event = factory(Event::class)->create();
        $event->addTickets(5, 'Type', 2500);
        $reservation = $event->reserveTickets(3, 'Type', 'john@example.com');
        $this->assertEquals(2, $event->ticketsRemaining());
        $this->assertEquals(7500, $reservation->totalCost());

        $reservation->cancel();

        $this->assertEquals(5, $event->ticketsRemaining());
    }

    /** @test */
    function retrieving_tickets_from_a_reservation()
    {
        $event = factory(Event::class)->create();
        $event->addTickets(10, 'Type', 2500);
        $tickets = $event->findTickets(4, 'Type');
        $reservation = new Reservation($tickets, 'john@example.com');

        $this->assertEquals($tickets, $reservation->tickets());
    }

    /** @test */
    function retrieving_the_email_from_a_reservation()
    {
        $ticket = factory(Ticket::class)->create();

        $reservation = new Reservation(collect($ticket), 'john@example.com');

        $this->assertEquals('john@example.com', $reservation->email());
    }

    /** @test */
    function completing_a_reservation()
    {
        $paymentGateway = new FakePaymentGateway;

        $event = factory(Event::class)->create();
        $tickets = factory(Ticket::class, 3)->create([
            'price' => 1500,
            'event_id' => $event->id
        ]);
        $reservation = new Reservation($tickets, 'john@example.com');

        $order = $reservation->complete($paymentGateway, $paymentGateway->getValidTestToken());

        $this->assertEquals('john@example.com', $order->email);
        $this->assertEquals(3, $order->ticketQuantity());
        $this->assertEquals(4500, $order->amount);
        $this->assertEquals(4500, $paymentGateway->totalCharges());
    }
}
