<?php

namespace Tests\Unit;

use App\Event;
use App\Exceptions\NotEnoughTicketsException;
use App\Facades\TicketCode;
use App\Order;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TicketTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    function a_ticket_can_be_reserved()
    {
        $ticket = factory(Ticket::class)->create();
        $this->assertNull($ticket->reserved_at);

        $ticket->reserve();

        $this->assertNotNull($ticket->fresh()->reserved_at);
    }
	
    /** @test */
    function a_ticket_can_be_released()
    {
        $ticket = factory(Ticket::class)->states('reserved')->create();
        $this->assertNotNull($ticket->fresh()->reserved_at);

        $ticket->release();

        $this->assertNull($ticket->fresh()->reserved_at);
    }

    /** @test */
    function a_ticket_can_be_claimed_for_an_order()
    {
        $order = factory(Order::class)->create();
        $ticket = factory(Ticket::class)->create(['code' => null]);
        TicketCode::shouldReceive('generateFor')->with($ticket)->andReturn('TICKETCODE1');

        $ticket->claimFor($order);

        $this->assertContains($ticket->id, $order->tickets->pluck('id'));
        $this->assertEquals('TICKETCODE1', $ticket->code);
    }

    /** @test */
    function a_ticket_has_a_type()
    {
        $ticket = factory(Ticket::class)->create(['type' => 'Test Ticket Type']);

        $this->assertEquals('Test Ticket Type', $ticket->type);
    }

    /** @test */
    function a_ticket_has_a_price()
    {
        $ticket = factory(Ticket::class)->create(['price' => 7000]);

        $this->assertEquals(7000, $ticket->price);
    }
}
